const { default: axios } = require('axios');

const router = require('express').Router();
const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

router.get('/create-event', (req, res) => {
  res.render('create-event', {
    title: 'create event',
  });
});
router.get('/event-payment', (req, res) => {
  res.render('event-payment', {
    title: 'Event Payment',
  });
});
// admin
router.get('/admin_login', (req, res) => {
  res.render('admin/admin_login', {
    title: 'Admin - Login',
  });
});

router.post('/', (req, res) => {
  axios
    .post(`${baseUrl}/auth/login`, req.body)
    .then((result) => {
      res.render('admin/admin_events', {
        title: 'Admin | Events',
        events: result.data.data,
        page: 'Admin',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/admin_events', (req, res) => {
  res.render('admin/admin_events', {
    title: 'Admin - Upcoming Events',
  });
});

router.get('/admin_lastevents', (req, res) => {
  res.render('admin/admin_lastevents', {
    title: 'Admin - Last Events',
  });
});

router.get('/admin_categories', (req, res) => {
  res.render('admin/admin_categories', {
    title: 'Admin Categories',
  });
});
router.get('/admin_articles', (req, res) => {
  res.render('admin/admin_articles', {
    title: 'Admin Articles',
  });
});
router.get('/admin_add-article', (req, res) => {
  res.render('admin/admin_add-article', {
    title: 'Admin add Articles',
  });
});
router.get('/admin_categories', (req, res) => {
  axios
    .get(`${baseUrl}/api/categories`)
    .then((result) => {
      res.render('admin/admin_categories', {
        title: 'Admin-Categories',
        events: result.data.data,
        page: 'Admin',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/admin_payment', (req, res) => {
  res.render('admin/admin_payment', {
    title: 'Admin Payment',
  });
});

router.get('/admin_payment_creator', (req, res) => {
  res.render('admin/admin_payment_creator', {
    title: 'Admin Payment Creator',
  });
});

module.exports = router;
