const { default: axios } = require('axios');

const router = require('express').Router();

const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

router.get('/detail_event/:id', (req, res) => {
  console.log(req.params.id);
  const { id } = req.params;
  axios
    .get(`${baseUrl}/api/events/${id}`)
    .then((result) => {
      res.render('event_detail', {
        title: 'Livent',
        events: result.data.data,
        page: 'Detail Event',
      });
    })

    .catch((err) => console.log(err));
});
router.get('/event_register', (req, res) => {
  res.render('event_register', {
    title: 'Event Register',
  });
});
router.get('/order_success', (req, res) => {
  res.render('order_success', {
    title: 'Order Success!',
  });
});
router.get('/detail_article', (req, res) => {
  res.render('article_detail', {
    title: 'Detail Article',
  });
});
router.get('/historyEvent', (req, res) => {
  res.render('historyEvent', {
    title: 'History Event',
  });
});
router.get('/eventCreator', (req, res) => {
  res.render('eventCreator', {
    title: 'History Event as Event Creator',
  });
});

module.exports = router;
