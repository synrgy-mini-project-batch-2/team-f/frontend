const { default: axios } = require('axios');

const router = require('express').Router();

const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

router.get('/', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('home', {
        title: 'Livent',
        events: result.data.data,
        page: 'Home',
      });
    })

    .catch((err) => console.log(err));
});

router.get('/', (req, res) => {
  axios
    .get(`${baseUrl}/api/articles`)
    .then((result) => {
      res.render('home', {
        title: 'Livent',
        articles: result.data.data,
        page: 'Home',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/home', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('home_user', {
        title: 'Livent',
        events: result.data.data,
        page: 'Home',
      });
    })

    .catch((err) => console.log(err));
});

router.get('/contactUs', (req, res) => {
  res.render('contactUs', {
    title: 'Contact Us',
  });
});

router.get('/error', (req, res) => {
  res.render('404', {
    title: 'Not Found',
  });
});

module.exports = router;
