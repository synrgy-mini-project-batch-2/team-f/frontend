const { default: axios } = require('axios');

const router = require('express').Router();
const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

router.get('/profile', (req, res) => {
  res.render('profileAccount', {
    title: 'Profile',
  });
});

router.get('/create-event', (req, res) => {
  res.render('create-event', {
    title: 'create event',
  });
});
router.get('/event-payment', (req, res) => {
  res.render('event-payment', {
    title: 'Event Payment',
  });
});
router.get('/historyEvent', (req, res) => {
  res.render('historyEvent', {
    title: 'History Event',
  });
});
router.get('/eventCreator', (req, res) => {
  res.render('eventCreator', {
    title: 'History Event as Event Creator',
  });
});
module.exports = router;
