const { default: axios } = require('axios');

const router = require('express').Router();

const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

// register
router.get('/signup', (req, res) => {
  res.render('signup', {
    title: 'Sign Up',
  });
});

router.post('/signup', (req, res) => {
  axios
    .post(`${baseUrl}/auth/register`, req.body)
    .then((result) => {
      res.redirect('/');
    })
    .catch((err) => console.log(err));
});
// end register

//   login
router.post('/', (req, res) => {
  axios.post(`${baseUrl}/auth/login`, req.body)
    .then((result) => {
      res.redirect('/home');
    })
    .catch((err) => console.log(err));
});

module.exports = router;
