const { default: axios } = require('axios');

const router = require('express').Router();
const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

module.exports = router;
