const { default: axios } = require('axios');

const router = require('express').Router();

const baseUrl = 'https://mini-project-f-backend.herokuapp.com';

// categories
router.get('/charity&causes', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('charity', {
        title: 'charity & causes',
        events: result.data.data,
        page: 'charity & causes',
      });
    })

    .catch((err) => console.log(err));
});

router.get('/entertainment', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('entertainment', {
        title: 'Entertainment',
        events: result.data.data,
        page: 'Entertainment',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/fashion', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('fashion', {
        title: 'Fashion',
        events: result.data.data,
        page: 'Fashion',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/finance', (req, res) => {
  res.render('finance', {
    title: 'finance & business',
  });
});
router.get('/finance', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('finance', {
        title: 'Finance',
        events: result.data.data,
        page: 'finance',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/hobies', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('hobies', {
        title: 'Hobies',
        events: result.data.data,
        page: 'Hobies',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/psychology', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('psychology', {
        title: 'Psychology',
        events: result.data.data,
        page: 'Psychology',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/all_events', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('all_events', {
        title: 'All Events',
        events: result.data.data,
        page: 'All Events',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/finance', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('finance', {
        title: 'Finance',
        events: result.data.data,
        page: 'finance',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/hobies', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('hobies', {
        title: 'Hobies',
        events: result.data.data,
        page: 'Hobies',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/psychology', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('psychology', {
        title: 'Psychology',
        events: result.data.data,
        page: 'Psychology',
      });
    })
    .catch((err) => console.log(err));
});

router.get('/all_events', (req, res) => {
  axios
    .get(`${baseUrl}/api/events`)
    .then((result) => {
      res.render('all_events', {
        title: 'All Events',
        events: result.data.data,
        page: 'All Events',
      });
    })
    .catch((err) => console.log(err));
});
// end categories

module.exports = router;
