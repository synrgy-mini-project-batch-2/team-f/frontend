# Development Guidelines (In Progress)

## 1. Project Initiation
---
```bash
# Change example.env to .env
mv example.env .env

# Install packages
npm install

# Create database
sequelize db:create

# Migrating the tables into database
sequelize db:migrate

# Seeding the default data
sequelize db:seed:all

# Run the dev script
npm run dev
```

## 2. Project Structure
---
```bash
├── config
|   ├── config.js
├── controllers
|   ├── feature-nameController.js
├── middlewares
|   ├── middleware-name.js
├── migrations
├── models
|   ├── model-name.js
├── routes
|   ├── route-nameRoute.js
├── views
|   ├── view-name.ejs
├── .env
├── index.js
├── package.json
```

## 3. API Specification
---

Visit this link to see the full API Specifications.
https://documenter.getpostman.com/view/12168381/Tz5qadAw