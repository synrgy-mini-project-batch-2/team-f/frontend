require("dotenv").config();
const jQuery = require("jquery");
const express = require("express");
const router = require("./routes/indexRoute");
const app = express();
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(express.static("views/assets/img"));
app.use(express.static("views/assets/style"));
app.use(express.static("views/assets/js"));
app.use(express.static("views/assets/img/icon"));
app.use("/uploads", express.static("https://mini-project-f-backend.herokuapp.com/uploads/banner"));

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static('views/assets/img'));
app.use(express.static('views/assets/style'));
app.use(express.static('views/assets/js'));
app.use(express.static('views/assets/img/icon'));
app.use('/uploads', express.static('https://mini-project-f-backend.herokuapp.com/uploads/banner'));

app.set('view engine', 'ejs');

app.use('/', require('./routes/indexRoute'));
app.use('/', require('./routes/adminRoute'));
app.use('/', require('./routes/authRoute'));
app.use('/', require('./routes/categoriesRoute'));
app.use('/', require('./routes/eventsRoute'));
app.use('/', require('./routes/rolesRoute'));
app.use('/', require('./routes/searchRoute'));
app.use('/', require('./routes/usersRoute'));

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is listening on http://localhost:${port}`);
});
